/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Enlace;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author REDP
 */
public class Conexion {

       public String driver = "com.mysql.jdbc.Driver";
       private String dbURL = "jdbc:mysql://localhost/Inmobiliaria";
       private String username = "root";
       private String password = "";
       private java.sql.Connection cnx; 
     

        public Conexion() {
            try
            {  
                Class.forName(driver).newInstance();          
                cnx= DriverManager.getConnection(dbURL,username,password);  
                if(cnx!=null)  
                {
                    System.out.println("Conectado");
                    cnx.createStatement();
          
                }

            }catch(SQLException ex)
            {
                ex.printStackTrace();
            }catch(Exception ex )
            {
                ex.printStackTrace(); 
            }
        }
        public void cerrarconexion() throws SQLException
        {
            cnx.close();
            cnx=null; 
        }
        public Connection getConexion(){
          return cnx;
        }

    
}
