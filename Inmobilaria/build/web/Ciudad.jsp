<%-- 
    Document   : Ciudad
    Created on : 3/05/2016, 12:05:24 PM
    Author     : Luisa
--%>

<%@page import="Control.controlCiudad"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.SQLException"%>
<%@page import="Control.controlEstado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <head>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="Stylesheet" href="css/style.css">
        <script src="js/jquery-1.12.2.min.js"></script>
   	<script src="js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inmobiliaria</title>
    </head>
    <body>
        <header>
            <h1>CIUDAD</h1>
        </header>
   
        <form action="ControladorCiudad" method="post">
       
            <ul class="ul">
                <li class="li">
                <label> Codigo </label>
                    <input type="text" name="txtCodigo">
                </li>
                <li class="li">
                <label> Nombre </label>
                    <input type="text" name="txtNombre">
                </li>
                <li class="li">
                <label> Estado </label>
                <select id="Combobox" name="txtEstado">
                 <%!
                    String driver = "com.mysql.jdbc.Driver";
                    String dbURL = "jdbc:mysql://localhost/Inmobiliaria";
                    String username = "root";
                    String password = "";
                    Connection con;  
                %>
                 <%
                      try 
                        {
                                Class.forName("com.mysql.jdbc.Driver").newInstance();
                                con= DriverManager.getConnection(dbURL,username,password);
                                Statement sst=con.createStatement();
                                ResultSet res=sst.executeQuery("SELECT  codigo_estado,nombre_estado FROM estado");
                                while(res.next()){
                                        %>
                                        <option value="<%=res.getLong("codigo_estado")%>">
                                        <%out.println(res.getString("nombre_estado")); 
                                                   
                                    %>
                                    </option>
                                    <%
                                 
                                    }
                           }
                           catch(SQLException ex)
                           {
                               out.println("Se daño sql");
                           }
                           %>
                </select>
                </li>
                <li class="li">
                    <input type="submit" name="BtnIngresar" value="Agregar"  class="boton">
               
                    <input type="submit" name="BtnModificar" value="Modificar"  class="boton">
                </li> 
            </ul>
        </form> 
                <% controlCiudad cct=new controlCiudad();
        %>
        <Table>
            <tr>
                <th>Código</th>
                <th>Nombre</th>  
                <th>Estado</th>  
                
            </tr>
            <% while(cct.getRs().next()){ %>
            <tr>
                <td><%= cct.getRs().getInt("codigo_ciudad")%></td>
                <td><%= cct.getRs().getString("nombre_ciudad")%></td>
                <td><%= cct.getRs().getString("fk_estado")%></td>
            </tr>
            <%}%>
        </Table> 
    </body>
</html>
