<%-- 
    Document   : Estado
    Created on : 3/05/2016, 09:32:30 AM
    Author     : Luisa
--%>

<%@page import="Control.controlEstado"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="Stylesheet" href="css/style.css">
        <script src="js/jquery-1.12.2.min.js"></script>
   	<script src="js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inmobiliaria</title>
    </head>
    <body>
        <header>
            <h1>ESTADO</h1>
        </header>
       <!-- <nav class="navbar navbar-inverse">
            <ul class="nav navbar-nav">
                <li><a href="Estado.jsp"</a> Estado</li>
                
            </ul>
        </nav>-->
        <form action="ControladorEstado" method="post">
       
            <ul class="ul">
                <li class="li">
                <label> Codigo </label>
                    <input type="text" name="txtCodigo">
                </li>
                <li class="li">
                <label> Nombre </label>
                    <input type="text" name="txtNombre">
                </li>
                <li class="li">
                    <input type="submit" name="BtnIngresar" value="Agregar"  class="boton">
               
                    <input type="submit" name="BtnModificar" value="Modificar"  class="boton">
                </li> 
            </ul>
        </form>  
        <% controlEstado cet=new controlEstado();
        %>
        <Table>
            <tr>
                <th>Código</th>
                <th>Nombre</th>                
            </tr>
            <% while(cet.getRs().next()){ %>
            <tr>
                <td><%= cet.getRs().getInt("codigo_estado")%></td>
                <td><%= cet.getRs().getString("nombre_estado")%></td>
            </tr>
            <%}%>
        </Table> 
    </body>
</html>
