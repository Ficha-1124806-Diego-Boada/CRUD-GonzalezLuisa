<%-- 
    Document   : Contrato
    Created on : 3/05/2016, 12:49:35 PM
    Author     : Luisa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="Stylesheet" href="css/style.css">
        <script src="js/jquery-1.12.2.min.js"></script>
   	<script src="js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inmobiliaria</title>
    </head>
    <body>
        <header>
            <h1>CIUDAD</h1>
        </header>
        <nav class="navbar navbar-inverse">
            <ul class="nav navbar-nav">
                <li><a href="Estado.jsp"</a> Estado</li>
     
                
            </ul>
        </nav>
        <form action="ControladorEstado" method="post">
       
            <ul class="ul">
                <li class="li">
                <label> Codigo </label>
                    <input type="text" name="txtCodigo">
                </li>
                <li class="li">
                <label> Fecha </label>
                    <input type="text" name="txtFecha">
                </li>
                <li class="li">
                <label> Especificaciones </label>
                    <input type="text" name="txtEspecificaciones">
                </li>
                <li class="li">
                <label> Presupuesto </label>
                    <input type="text" name="txtPresupuesto">
                </li>
                <li class="li">
                <label> Cliente </label>
                    <input type="text" name="txtCliente" list="listCliente">
                </li>
                <li class="li">
                <label> Empleado </label>
                    <input type="text" name="txtEmpleado" list="listEmplado">
                </li>
                <li class="li">
                <label> Tipo contrato </label>
                    <input type="text" name="txtTipo" list="listTipo">
                </li>
                <li class="li">
                <label> Estado </label>
                    <input type="text" name="txtEstado" list="listEstado">
                </li>
                <li class="li">
                    <input type="submit" name="BtnIngresar" value="Agregar"  class="boton">
               
                    <input type="submit" name="BtnModificar" value="Modificar"  class="boton">
                </li> 
            </ul>
        </form> 
    </body>
</html>
