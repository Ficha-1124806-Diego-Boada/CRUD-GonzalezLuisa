<%-- 
    Document   : Cliente
    Created on : 3/05/2016, 12:42:45 PM
    Author     : Luisa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link rel="Stylesheet" href="css/style.css">
        <script src="js/jquery-1.12.2.min.js"></script>
   	<script src="js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inmobiliaria</title>
    </head>
    <body>
        <header>
            <h1>CLIENTE</h1>
        </header>
        <nav class="navbar navbar-inverse">
            <ul class="nav navbar-nav">
                <li><a href="Estado.jsp"</a> Estado</li>
            </ul>
        </nav>
        <form action="ControladorEstado" method="post">
       
            <ul class="ul">
                <li class="li">
                <label> Cedula </label>
                    <input type="text" name="txtCedula">
                </li>
                <li class="li">
                <label> Nombre </label>
                    <input type="text" name="txtNombre">
                </li>
                <li class="li">
                <label> Apellido </label>
                    <input type="text" name="txtApellido">
                </li>
                <li class="li">
                <label> Fecha de nacimiento </label>
                    <input type="text" name="txtFecha">
                </li>
                 <li class="li">
                <label> Edad </label>
                    <input type="text" name="txtEdad">
                </li>
                 <li class="li">
                <label> Genero </label>
                    <input type="text" name="txtGenero">
                </li>
                 <li class="li">
                <label> Nombre referido </label>
                    <input type="text" name="txtNombreReferido">
                </li>
                <li class="li">
                    <label> Ciudad </label>
                    <input type="text" name="txtCiudad" list="listCiudad">
                </li>
                <label> Estado </label>
                    <input type="text" name="txtEstado" list="listEstado">
                </li>
                <li class="li">
                    <input type="submit" name="BtnIngresar" value="Agregar"  class="boton">
               
                    <input type="submit" name="BtnModificar" value="Modificar"  class="boton">
                </li> 
            </ul>
        </form> 
    </body>
</html>
